import React, { useState, useEffect } from 'react';
import { socketEmitEvent } from '../../../utils/socketEvent';
import './Cabinet.scss';
import Cabinets from './Cabinets';
import CabinetOut from './CabinetOut';
const Cabinet = props => {
  const { requestId, authenticated, socket } = props;

  const [cabinets, setCabinets] = useState([]);
  useEffect(() => {
    // Get list cabinet
    if (authenticated) {
      let data = {
        requestId,
        appServiceType: 21
      };

      socketEmitEvent(socket, data);
    }
  }, [authenticated, requestId, socket]);

  useEffect(() => {
    if (socket) {
      socket.on('connect', () => {
        socket.on('chatevent', res => {
          const { appServiceType, result, data: resData } = JSON.parse(res);
          const data = JSON.parse(resData);
          if (appServiceType === 21) {
            setCabinets(data.cabinets);
          }
        });
      });
    }
  }, [requestId, socket]);

  return (
    <div className="cabinet">
      {cabinets.length < 1 ? (
        <div className="loading" />
      ) : (
        // <Cabinets cabinets={cabinets} />
        <CabinetOut cabinet={cabinets[0]} />
      )}
    </div>
  );
};

Cabinet.propTypes = {};

export default Cabinet;
