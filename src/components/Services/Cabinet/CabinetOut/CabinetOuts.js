import React from 'react';
import PropTypes from 'prop-types';
import Tabs from '../../../hoc/Tabs/Tabs';
import Engine from './Engine';
import Light from './Light';

const CabinetOuts = props => {
  const { cabinetOuts } = props;

  const Engines = cabinetOuts.filter(cbnOuts => cbnOuts.type === 'engine');
  const Lights = cabinetOuts.filter(cbnOuts => cbnOuts.type === 'flashlight');

  return (
    <div>
      <Tabs>
        <div label="Động cơ">
          <div className="container">
            {Engines.map(cbn => (
              <div className="item_cbo" key={cbn.indexOf}>
                <Engine cabinet={cbn} />
              </div>
            ))}
          </div>
        </div>
        <div label="Đèn">
          <div className="container">
            {Lights.map(cbn => (
              <div className="item_cbo" key={cbn.indexOf}>
                <Light cabinet={cbn} />
                <div className="detail">
                </div>
              </div>
            ))}
          </div>
        </div>
      </Tabs>
    </div>
  );
};

CabinetOuts.propTypes = {
  cabinetOuts: PropTypes.array.isRequired
};

export default CabinetOuts;
