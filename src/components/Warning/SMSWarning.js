import React, { useState, useEffect } from 'react';
import { socketEmitEvent, socketGetWarning } from '../../utils/socketEvent';
import { css } from 'glamor';
import { toast } from 'react-toastify';
import compareErrors from '../../utils/compareErrors';
import './Warnings.scss';

const SMSWarning = props => {
  const { requestId, authenticated, socket } = props;
  const [countLoad, setCountLoad] = useState(0);
  const [process, setProcess] = useState(false);

  const [configCabinet, setConfigCabinet] = useState([]);
  const [configCamera, setConfigCamera] = useState([]);
  const [configSensor, setConfigSensor] = useState([]);

  useEffect(() => {
    if (authenticated) {
      socketGetWarning(socket, requestId, 406, 1);

      socketGetWarning(socket, requestId, 406, 5);

      socketGetWarning(socket, requestId, 406, 3);
    }
  }, [authenticated, requestId, socket]);

  useEffect(() => {
    if (socket) {
      socket.on('connect', () => {
        socket.on('chatevent', res => {
          const { appServiceType, result, data: resData } = JSON.parse(res);
          const data = JSON.parse(resData);

          if (appServiceType === 405) {
            setProcess(false);
            const state = data.action === 1 ? 'Bật' : 'Tắt';
            if (result === 1) {
              toast(`${state} thông báo thành công.`, {
                className: css({
                  background: '#22b573',
                  color: '#ffffff'
                }),
                position: toast.POSITION.TOP_CENTER
              });
            } else {
              const { deviceTypeId } = data;
              socketGetWarning(socket, requestId, 406, deviceTypeId);
              toast('Cập nhật thất bại, vui lòng thử lại!', {
                className: css({
                  background: '#22b573',
                  color: '#ffffff'
                }),
                position: toast.POSITION.TOP_CENTER
              });
            }
          }

          if (appServiceType === 406 && data.length > 0) {
            if (data[0].deviceTypeId === 1) {
              setCountLoad(state => state + 1);
              setConfigCabinet(data);
            }
            if (data[0].deviceTypeId === 3) {
              setCountLoad(state => state + 1);
              setConfigCamera(data);
            }
            if (data[0].deviceTypeId === 5) {
              setCountLoad(state => state + 1);
              setConfigSensor(data);
            }
          }
        });
      });
    }
  }, [requestId, socket]);
  const handleServiceSMS = (deviceTypeId, configError) => {
    if (authenticated && !process) {
      // TURN-ON : action = 1
      // TURN-OFF : action = 2
      // isSms = 1 (on)
      // SMS : alertType = 1
      // CALL : alertType = 2
      setProcess(true);
      const { isSms, errorId } = configError;
      const action = isSms === 1 ? 2 : 1;
      const data = {
        requestId,
        appServiceType: 405,
        body: JSON.stringify({
          errorId,
          deviceTypeId,
          alertType: 1,
          action
        })
      };

      socketEmitEvent(socket, data);
      if (deviceTypeId === 1) {
        setConfigCabinet(state => [
          ...state.filter(val => val.errorId !== errorId),
          { ...configError, isSms: Math.abs(isSms - 1) }
        ]);
      }
      if (deviceTypeId === 5) {
        setConfigSensor(state => [
          ...state.filter(val => val.errorId !== errorId),
          { ...configError, isSms: Math.abs(isSms - 1) }
        ]);
      }
      if (deviceTypeId === 3) {
        setConfigCamera(state => [
          ...state.filter(val => val.errorId !== errorId),
          { ...configError, isSms: Math.abs(isSms - 1) }
        ]);
      }
    }
  };
  return (
    <div>
      {countLoad < 2 ? (
        <div className="loading" />
      ) : (
        <ul className="service">
          <li className="cabinet">
            <div className="title paddingLeft8">Hệ thống tủ điện</div>
            <ul className="main">
              {configCabinet.sort(compareErrors).map(val => (
                <li key={val.errorId} className="child">
                  <div className="content">{val.title}</div>
                  <div className="change">
                    <label className="switch">
                      <input
                        type="checkbox"
                        checked={val.isSms === 1}
                        onChange={() => handleServiceSMS(1, val)}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div>
                </li>
              ))}
            </ul>
          </li>
          <li className="sensor">
            <div className="title">Cảm biến môi trường</div>
            <ul className="main">
              {configSensor.sort(compareErrors).map(val => (
                <li key={val.errorId} className="child">
                  <div className="content">{val.title}</div>
                  <div className="change">
                    <label className="switch">
                      <input
                        type="checkbox"
                        checked={val.isSms === 1}
                        onChange={() => handleServiceSMS(5, val)}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div>
                </li>
              ))}
            </ul>
          </li>
          <li className="camera">
            <div className="title">Hệ thống camera</div>
            <ul className="main">
              {configCamera.sort(compareErrors).map(val => (
                <li key={val.errorId} className="child">
                  <div className="content">{val.title}</div>
                  <div className="change">
                    <label className="switch">
                      <input
                        type="checkbox"
                        checked={val.isSms === 1}
                        onChange={() => handleServiceSMS(3, val)}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div>
                </li>
              ))}
            </ul>
          </li>
        </ul>
      )}
    </div>
  );
};

export default SMSWarning;
