import React from 'react';
import PropTypes from 'prop-types';

const Tab = props => {
  const { activeTab, label, onClick } = props;

  let className = 'tab-list-item';

  if (activeTab === label) {
    className += ' tab-list-active';
  }
  const onClickCustom = () => {
    onClick(label);
  };
  return (
    <li className={className} onClick={onClickCustom}>
      {label}
    </li>
  );
};

Tab.propTypes = {
  activeTab: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Tab;
