const compareErrors = (objA, objB) => {
  if (objA.errorId < objB.errorId) {
    return -1;
  }
  if (objA.errorId > objB.errorId) {
    return 1;
  }
  return 0;
};

export default compareErrors;
