import React from 'react';
import PropTypes from 'prop-types';
import './CabinetOut.scss';
import Icon, { Stack } from '@mdi/react';
import {
  mdiCircleOutline,
  mdiAmplifier,
  mdiEngine,
  mdiLightbulb
} from '@mdi/js';
import CabinetOuts from './CabinetOuts';

function CabinetOut(props) {
  const { cabinet } = props;

  return (
    <div>
      <div className="cbnOut" onClick={() => console.log(cabinet)}>
        <div className="leftside">
          <div className="icon">
            <Stack size={4.5}>
              <Icon path={mdiAmplifier} color="#ffffff" size={2.5} />
              <Icon path={mdiCircleOutline} color="#ffffff" />
            </Stack>
          </div>
          <div className="text">Serial: {cabinet.serial}</div>
          <div className="text">
            NSX:{' '}
            {cabinet && new Date(cabinet.createdAt).toLocaleDateString('en-GB')}
          </div>
        </div>
        <div className="rightside">
          <div className="state">
            {parseInt(cabinet.state) === 0
              ? 'Đang hoạt động'
              : 'Đang ngừng hoạt động'}
          </div>
          <div className="engines">
            <div className="engine">
              {cabinet &&
                cabinet.cabinetOuts.filter(cbnOuts => cbnOuts.type === 'engine')
                  .length}{' '}
              <Icon path={mdiEngine} size={0.8} color="#22b573" />
            </div>
            <div className="engine">
              {cabinet &&
                cabinet.cabinetOuts.filter(
                  cbnOuts => cbnOuts.type === 'flashlight'
                ).length}{' '}
              <Icon path={mdiLightbulb} size={0.8} color="#22b573" />
            </div>
          </div>
          <div className="data">
            <table>
              <tbody>
                <tr>
                  <td className="right">Điện tiêu thụ</td>
                  <td className="left">{cabinet && cabinet.power} Wh </td>
                </tr>
                <tr>
                  <td className="right">Dòng điện</td>
                  <td className="left">
                    {cabinet && cabinet.i1}.{cabinet && cabinet.i2}.
                    {cabinet && cabinet.i3} A{' '}
                    <span className="mdi mdi-home"></span>
                  </td>
                </tr>
                <tr>
                  <td className="right">Hiệu điện thế</td>
                  <td className="left">
                    {cabinet && `${cabinet.u1}.${cabinet.u2}.${cabinet.u3}`} V
                  </td>
                </tr>
                <tr>
                  <td className="right">Nhiệt độ tủ</td>
                  <td className="left">
                    {cabinet && cabinet.temperature} <span>&#8451;</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {cabinet.cabinetOuts && <CabinetOuts cabinetOuts={cabinet.cabinetOuts} />}
    </div>
  );
}

CabinetOut.propTypes = {
  cabinet: PropTypes.object.isRequired
};

export default CabinetOut;
