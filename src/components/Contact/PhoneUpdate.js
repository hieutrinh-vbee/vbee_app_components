import React from 'react';
import PropTypes from 'prop-types';

import Modal from '../../components/hoc/Modal/Modal';

const PhoneUpdate = props => {
  const { show, phone, setPhone, setShow, handleUpdatePhone } = props;

  return (
    <Modal show={show}>
      <div className="phonetitles">Cập nhật số điện thoại</div>
      <div className="phoneform">
        <form onSubmit={event => handleUpdatePhone(event)}>
          <div className="inputs">
            <input
              className="inputPhone"
              type="text"
              name="phone"
              value={phone}
              onChange={event => setPhone(event.target.value)}
            />
          </div>

          <div className="buttons">
            <div className="left" onClick={() => setShow(false)}>
              Hủy
            </div>
            <div className="right">
              <input type="submit" className="submit" value="Cập nhật" />
            </div>
          </div>
        </form>
      </div>
    </Modal>
  );
};

PhoneUpdate.propTypes = {
  show: PropTypes.bool.isRequired,
  phone: PropTypes.string.isRequired,
  requestId: PropTypes.string.isRequired,
  process: PropTypes.bool.isRequired,
  socket: PropTypes.object.isRequired
};

export default PhoneUpdate;
