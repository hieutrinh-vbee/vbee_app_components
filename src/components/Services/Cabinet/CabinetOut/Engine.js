import React from 'react';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiClock, mdiEngine, mdiPower } from '@mdi/js';

const Engine = props => {
  const { cabinet } = props;
  const { state } = cabinet;
  if (state === 1) {
    return (
      <div className="engine-on">
        <div className="head">
          <div className="clock">
            <Icon path={mdiClock} color="#22b573" size={1.2} />
          </div>
          <div className="name_">{cabinet.name}</div>
          <div className="index">{cabinet.indexOf}</div>
        </div>
        <div className="main">
          <Icon path={mdiEngine} color="#22b573" size={4} />
        </div>
        <div className="foot">
          <div className="switch">
            <Icon path={mdiPower} color="#ffffff" size={1.2} />
          </div>
          <div className="content">Bấm để tắt</div>
        </div>
      </div>
    );
  }
  return (
    <div className="engine-off">
      <div className="head">
        <div className="clock">
          <Icon path={mdiClock} color="#686868" size={1.2} />
        </div>
        <div className="name_">{cabinet.name}</div>
        <div className="index">{cabinet.indexOf}</div>
      </div>
      <div className="main">
        <Icon path={mdiEngine} color="#686868" size={4} />
      </div>
      <div className="foot">
        <div className="content">Bấm để bật</div>
        <div className="switch">
          <Icon path={mdiPower} color="#ffffff" size={1.2} />
        </div>
      </div>
    </div>
  );
};

Engine.propTypes = {
  cabinet: PropTypes.object.isRequired
};

export default Engine;
