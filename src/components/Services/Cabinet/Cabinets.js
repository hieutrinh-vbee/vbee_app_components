import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Icon, { Stack } from '@mdi/react';
import { mdiWifiOff, mdiPlus, mdiCircleOutline } from '@mdi/js';
import Cabinet from './Cabinet';
const Cabinets = props => {
  const { cabinets } = props;
  const [picked, setPicked] = useState();
  useEffect(() => {
    setPicked(cabinets[0]);
  }, [cabinets]);
  return (
    <div>
      <div className="picked">
        <div className="leftside">
          <div className="temperature">
            {picked && picked.temperature}
            <span>&#8451;</span>
          </div>
          <div className="note">Nhiệt độ ngoài trời</div>
        </div>
        <div className="rightside">
          <div className="data">
            <table>
              <tbody>
                <tr>
                  <td className="right">Điện tiêu thụ</td>
                  <td className="left">{picked && picked.power} Wh </td>
                </tr>
                <tr>
                  <td className="right">Dòng điện</td>
                  <td className="left">
                    {picked && picked.i1}.{picked && picked.i2}.
                    {picked && picked.i3} A <span className="mdi mdi-home"></span>
                  </td>
                </tr>
                <tr>
                  <td className="right">Công suất</td>
                  <td className="left">{picked && picked.p} W</td>
                </tr>
                <tr>
                  <td className="right">Tại</td>
                  <td className="left">{picked && picked.name}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="detail">
            <div className="btn">Chi tiết</div>
          </div>
        </div>
      </div>
      <div className="middle">
        <div className="title">
          <div className="text">Danh sách tủ điện</div>
        </div>
        <div className="add">
          <div className="btn">
            <Stack style={{ cursor: 'pointer' }} size={1.5}>
              <Icon path={mdiPlus} color="#22b573" size={1.2} />
              <Icon path={mdiCircleOutline} color="#22b573" />
            </Stack>
          </div>
        </div>
      </div>
      <div className="container">
        {cabinets.map(cbn => (
          <div className="item" key={cbn.serial}>
            <div className="name">{cbn.name}</div>
            {cbn.state === 1 ? (
              <div className="off">
                <Icon path={mdiWifiOff} size={4} color="#686868" />
              </div>
            ) : (
              <Cabinet cabinet={cbn} />
            )}
            <div className="detail">
              <div className="btn">Chi tiết</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

Cabinets.propTypes = {
  cabinets: PropTypes.array.isRequired
};

export default Cabinets;
