export const socketEmitEvent = (socket, data) => {
  console.log('Emit:', data);
  return socket.emit('chatevent', data);
};

export const socketGetWarning = (
  socket,
  requestId,
  appServiceType,
  deviceTypeId
) => {
  // console.log('Emit:', { appServiceType, deviceTypeId });
  const data = {
    requestId,
    appServiceType,
    body: JSON.stringify({
      deviceTypeId
    })
  };
  return socket.emit('chatevent', data);
};
