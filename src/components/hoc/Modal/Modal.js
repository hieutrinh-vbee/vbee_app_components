import React from 'react';
import './Modal.scss';

const Modal = ({ show, children }) => {
  const name = show ? 'modal display-block' : 'modal display-none';
  return (
    <div className={name}>
      <section className="modal-main">{children}</section>
    </div>
  );
};

export default Modal;
