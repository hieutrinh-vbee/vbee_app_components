import React from 'react';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiEngine, mdiLightbulb } from '@mdi/js';

const Cabinet = props => {
  const { cabinet } = props;
  return (
    <div className="data">
      <table>
        <tbody>
          <tr>
            <td className="right">Động cơ</td>
            <td className="left">
              <div className="val">
                <div className="num">
                  {
                    cabinet.cabinetOuts.filter(
                      cabintOut => cabintOut.type === 'engine'
                    ).length
                  }
                </div>
                <div className="ico">
                  <Icon path={mdiEngine} size={0.7} color="#22b573" />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td className="right">Đèn</td>
            <td className="left">
              <div className="val">
                <div className="num">
                  {
                    cabinet.cabinetOuts.filter(
                      cabintOut => cabintOut.type === 'flashlight'
                    ).length
                  }
                </div>
                <div className="ico">
                  <Icon path={mdiLightbulb} size={0.7} color="#22b573" />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td className="right">Dòng điện</td>
            <td className="left">
              {cabinet.i1}.{cabinet.i2}.{cabinet.i3} A
            </td>
          </tr>
          <tr>
            <td className="right">Tiêu thụ</td>
            <td className="left">{cabinet.power} Wh</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

Cabinet.propTypes = {
  cabinet: PropTypes.object.isRequired
};

export default Cabinet;
