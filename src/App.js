import React, { useEffect, useState } from 'react';
import io from 'socket.io-client';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';
import { css } from 'glamor';
import { socketEmitEvent } from './utils/socketEvent';
import SMSWarning from './components/Warning/SMSWarning';
import CallWarning from './components/Warning/CallWarning';
import Phone from './components/Contact/Phone';
import Cabinet from './components/Services/Cabinet';

const App = () => {
  const [socket, setSocket] = useState(undefined);

  const [requestId, setRequestId] = useState();
  const [authenticated, setAuthenticated] = useState(false);
  const [services, setServices] = useState({});

  useEffect(() => {
    setSocket(io.connect('http://183.91.11.21:9092'));
    setRequestId(Math.floor(100000000 + Math.random() * 900000000));
  }, []);
  useEffect(() => {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let accessToken = params.get('accessToken');
    let type = params.get('type');
    let id = params.get('id');
    setServices({ type, id });

    if (requestId) {
      if (!accessToken) {
        toast('Tài khoản chưa thiết lập, vui lòng thử lại!', {
          position: toast.POSITION.TOP_CENTER,
          autoClose: false
        });
      } else {
        const data = {
          requestId,
          appServiceType: 1,
          body: '{accessToken: ' + accessToken + '}'
        };

        socketEmitEvent(socket, data);
      }
    }
  }, [requestId, socket]);

  useEffect(() => {
    if (socket) {
      socket.on('connect', () => {
        socket.on('chatevent', res => {
          const { appServiceType, result } = JSON.parse(res);

          if (appServiceType === 1) {
            if (result === 1) {
              return setAuthenticated(true);
            }
            toast('Tài khoản chưa thiết lập, vui lòng thử lại!', {
              position: toast.POSITION.TOP_CENTER,
              autoClose: false
            });
          }
        });
      });
      socket.on('disconnect', () => setRequestId(undefined));
    }
  }, [requestId, socket]);

  return (
    <div className="container">
      <ToastContainer
        autoClose={2500}
        progressClassName={css({
          height: 0
        })}
      />
      {services.type === 'service' && services.id === '1' ? (
        <SMSWarning
          requestId={requestId}
          socket={socket}
          authenticated={authenticated}
        />
      ) : services.id === '5' ? (
        <>
          <Phone
            requestId={requestId}
            socket={socket}
            authenticated={authenticated}
          />
          <CallWarning
            requestId={requestId}
            socket={socket}
            authenticated={authenticated}
          />
        </>
      ) : services.id === '21' ? (
        <Cabinet
          requestId={requestId}
          socket={socket}
          authenticated={authenticated}
        />
      ) : (
        undefined
      )}
    </div>
  );
};

export default App;
