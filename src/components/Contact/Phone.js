import React, { useEffect, useState } from 'react';
import PhoneUpdate from './PhoneUpdate';
import { socketEmitEvent } from '../../utils/socketEvent';
import { css } from 'glamor';
import { toast } from 'react-toastify';
import phoneValidate from '../../utils/phoneValidate';
import './Phone.scss';

const Phone = props => {
  const { requestId, authenticated, socket } = props;

  const [show, setShow] = useState(false);
  const [phone, setPhone] = useState('');
  const [process, setProcess] = useState(false);

  useEffect(() => {
    if (authenticated) {
      let reqUser = {
        requestId,
        appServiceType: 3
      };
      socketEmitEvent(socket, reqUser);
    }
  }, [authenticated, requestId, socket]);

  useEffect(() => {
    if (socket) {
      socket.on('connect', () => {
        socket.on('chatevent', res => {
          const { appServiceType, result, data: resData } = JSON.parse(res);
          const data = JSON.parse(resData);

          if (appServiceType === 3) {
            if (phone === '') {
              if (data.contact) {
                setPhone(data.contact);
              } else if (data.phoneNumber) {
                setPhone(data.phoneNumber);
              }
            }
          }
          if (appServiceType === 4) {
            setProcess(false);
            if (result === 1) {
              toast('Cập nhật số điện thoại thành công.', {
                className: css({
                  background: '#22b573',
                  color: '#ffffff'
                }),
                position: toast.POSITION.TOP_CENTER
              });
              setShow(false);
            } else {
              toast('Cập nhật thất bại, vui lòng thử lại', {
                className: css({
                  background: '#22b573',
                  color: '#ffffff'
                }),
                position: toast.POSITION.TOP_CENTER
              });
            }
          }
        });
      });
    }
  }, [phone, socket]);

  const handleUpdatePhone = event => {
    event.preventDefault();
    if (phoneValidate(phone) && !process) {
      let data = {
        requestId,
        appServiceType: 4,
        body: JSON.stringify({
          contact: phone
        })
      };
      socketEmitEvent(socket, data);
    } else {
      toast('Số điện thoại không hợp lệ!', {
        className: css({
          background: '#22b573',
          color: '#ffffff'
        }),
        position: toast.POSITION.TOP_CENTER
      });
    }
  };

  return (
    <div>
      <PhoneUpdate
        show={show}
        phone={phone}
        setPhone={setPhone}
        setShow={setShow}
        handleUpdatePhone={handleUpdatePhone}
      />
      <div className="contact margin3">
        <div className="phone"> Liên hệ: {phone}</div>
        <div className="update" onClick={() => setShow(true)}>
          thay đổi
        </div>
      </div>
    </div>
  );
};

export default Phone;
